#!/bin/bash

#cat /etc/passwd | grep '/bin/bash' | cut -d: -f1

file="/etc/passwd"
while IFS=: read -r f1 f2 f3 f4 f5 f6 f7
do
	
        # display fields using f1, f2,..,f7
	if grep -Fxq $f7 /etc/shells
	then
    		# code if found
		printf '%s  %s \n' "$f1" "$f7"
	fi
done <"$file"
